require_relative "../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      class Events < Cog::Command
        def run_command
          username = Args.required(request.args[0], "Username is required to perform a search")
          events = GitLab::API::UserClient.new.get_events(username)
          response.template = "user_events"
          response.content = events.to_a
        end
      end
    end
  end
end
