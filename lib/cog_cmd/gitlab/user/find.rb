require_relative "../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      class Find < Cog::Command
        def run_command
          username = Args.required(request.args[0], "Username is required to perform a search")
          users = GitLab::API::UserClient.new.find(username).to_a
          response.template = "user_find"
          response.content = users.to_a
        end
      end
    end
  end
end
