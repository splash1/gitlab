require_relative "../helper"
require "api/runners"

module CogCmd
  module Gitlab
    module Runners
      class Pause < Cog::Command
        def run_command
          runner_id = Args.required(request.args[0], "I need a Runner ID to pause a Runner")
          runner = GitLab::API::RunnersClient.new.pause(runner_id)
          response.template = "runners_active_state_change"
          response.content = runner.to_h
        end
      end
    end
  end
end
