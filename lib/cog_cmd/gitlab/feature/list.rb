require_relative "../helper"
require "api/feature"

module CogCmd
  module Gitlab
    module Feature
      class List < Cog::Command
        def run_command
          features = GitLab::API::FeatureClient.new.list

          response.template = "feature_list"
          response.content = features.to_a
        end
      end
    end
  end
end
