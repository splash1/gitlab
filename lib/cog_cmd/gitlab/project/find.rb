require_relative "../helper"
require "api/project"

module CogCmd
  module Gitlab
    module Project
      class Find < Cog::Command
        def run_command
          project_path = Args.required(request.args[0], "I need a project path to perform a search")
          project = GitLab::API::ProjectClient.new.find(project_path)
          response.template = "project_find"
          response.content = project.to_a
        end
      end
    end
  end
end
