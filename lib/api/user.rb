require_relative "client"

module GitLab
  module API
    User = Struct.new(:id, :username, :name, :email, :state, :created_at,
                      :two_factor_enabled, :last_sign_in_at, :current_sign_in_at) do
      def exists?
        true
      end

      def to_a
        [to_h]
      end
    end

    Event = Struct.new(:created_at, :action_name, :target_type, :target_title) do
      def exists?
        true
      end

      def to_a
        [to_h]
      end
    end

    NullUser = Struct.new(:username) do
      def exists?
        false
      end

      def to_a
        []
      end
    end

    NullEvent = Struct.new(:username) do
      def exists?
        false
      end

      def to_a
        []
      end
    end

    class UserClient
      def initialize(client = Client.new)
        @client = client
      end

      def find(username)
        fail "A username is required" if username.nil? || username.empty?
        response = @client.get("/api/v4/users?username=#{CGI.escape(username)}")
        case response.code.to_i
        when 200
          users = JSON.parse(response.body)
          if users.empty?
            NullUser.new(username)
          else
            User.from_hash(users[0])
          end
        when 404
          NullUser.new(username)
        else
          fail "Failed to get user #{username}: #{response.code} - #{response.message}"
        end
      end

      def block(username)
        user = find(username)
        fail user.to_s unless user.exists?
        response = @client.put("/api/v4/users/#{user.id}/block")
        fail "Cannot block user #{username}: #{response.code} - #{response.body}" unless response.code == "200"
      end

      def unblock(username)
        user = find(username)
        fail user.to_s unless user.exists?
        response = @client.put("/api/v4/users/#{user.id}/unblock")
        fail "Cannot block user #{username}: #{response.code} - #{response.body}" unless response.code == "200"
      end

      def get_last_event(username)
        get_events(username, 1)[0]
      end

      def get_events(username, limit = 30)
        user = find(username)
        fail user.to_s unless user.exists?
        response = @client.get("/api/v4/users/#{user.id}/events?per_page=#{limit}")
        case response.code.to_i
        when 200
          events = JSON.parse(response.body)
          if events.empty?
            [NullEvent.new(username)]
          else
            events.map { |event| Event.from_hash(event) }
          end
        when 404
          [NullEvent.new(username)]
        else
          fail "Failed to get Events for user #{username}: #{response.code} - #{response.message}"
        end
      end

      def update_email(username, new_email)
        user = find(username)
        fail user.to_s unless user.exists?
        response = @client.put("/api/v4/users/#{user.id}?email=#{new_email}")
        fail_msg = "Cannot change user #{username}'s email to #{new_email}: #{response.code} - #{response.body}"
        fail fail_msg unless response.code == "200"
      end
    end
  end
end
