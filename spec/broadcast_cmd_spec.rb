require 'spec_helper'
require 'cog_cmd/gitlab/broadcast'

describe "broadcast message" do
  # This is so because gitlab-ci is not a tty and cog tries to read from it failing
  before { allow(STDIN).to receive(:tty?) { true } }

  it "fails with an error without a message" do
    broadcaster = CogCmd::Gitlab::Broadcast.new
    expect { broadcaster.run_command }.to raise_error /Can\'t set broadcast without a message/
  end

  it "sets a message when there's a message" do
    req = stub_request(:post, /api\/v4\/broadcast_messages/).
      with(body: "message=my new message").
      to_return( {status: 200,
                  body: { message: "my message",
                          starts_at: "now",
                          ends_at: "1h",
                          id: 1,
                          active: true }.to_json })

    with_environment("my new message") do
      broadcaster = CogCmd::Gitlab::Broadcast.new
      broadcaster.run_command
      expect(broadcaster.response.content).to match /Broadcast message set to 'my message'/
    end
    remove_request_stub(req)
  end
end
