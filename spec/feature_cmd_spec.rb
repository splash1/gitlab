require 'spec_helper'
require 'cog_cmd/gitlab/feature/list'
require 'cog_cmd/gitlab/feature/set'

describe 'features commands' do
  let(:feature1) do
    {
      name: 'experimental_feature',
      state: 'on',
      gates: [{ 'key' => 'boolean', 'value' => true }],
      gate_list: 'boolean: true'
    }
  end
  let(:feature2) do
    {
      name: 'new_library',
      state: 'off',
      gates: [{ 'key' => 'boolean', 'value' => false }],
      gate_list: 'boolean: false'
    }
  end
  let(:features) { [feature1, feature2] }

  before { allow(STDIN).to receive(:tty?) { true } }

  describe 'list command' do
    before do
      @request = stub_request(:get, /api\/v4\/features/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda { |request| { status: 200, body: features.to_json } }
    end

    after do
      remove_request_stub(@request)
    end

    it 'returns a list of features' do
      list = CogCmd::Gitlab::Feature::List.new
      list.run_command

      content = list.response.content
      expect(content.count).to be(2)
      expect(content).to include(feature1)
      expect(content).to include(feature2)
      expect(list.response.template).to eq('feature_list')
    end
  end

  describe 'set command' do
    let(:feature2_updated) do
      {
        name: 'new_library',
        state: 'conditional',
        gates: [
          { 'key' => 'boolean', 'value' => false },
          { 'key' => 'percentage_of_time', 'value' => 30 }
        ],
        gate_list: 'boolean: false, percentage_of_time: 30'
      }
    end

    before do
      update = lambda do |request|
        feature2[:state] = 'conditional'
        feature2[:gates] = [
          { 'key' => 'boolean', 'value' => false },
          { 'key' => 'percentage_of_time', 'value' => 30 }
        ]

        { status: 201, body: feature2.to_json }
      end

      @request = stub_request(:post, /api\/v4\/features\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).to_return update
    end

    after do
      remove_request_stub(@request)
    end

    it 'returns a list of features' do
      with_environment(feature2[:name], '30') do
        setter = CogCmd::Gitlab::Feature::Set.new
        setter.run_command

        content = setter.response.content
        expect(content.count).to be(1)
        expect(content).to include(feature2_updated)
        expect(setter.response.template).to eq('feature_list')
      end
    end
  end
end
