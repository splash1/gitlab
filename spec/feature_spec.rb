require 'spec_helper'
require 'api/feature'

module GitLab
  module API
    describe FeatureClient do
      let(:feature1) do
        {
          name: 'experimental_feature',
          state: 'on',
          gates: [{ 'key' => 'boolean', 'value' => true }],
          gate_list: 'boolean: true'
        }
      end
      let(:feature2) do
        {
          name: 'new_library',
          state: 'off',
          gates: [{ 'key' => 'boolean', 'value' => false }],
          gate_list: 'boolean: false'
        }
      end
      let(:features) { [feature1, feature2] }

      before { allow(STDIN).to receive(:tty?) { true } }

      describe '#list' do
        context 'valid request' do
          before do
            @request = stub_request(:get, /api\/v4\/features/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda { |request| { status: 200, body: features.to_json } }
          end

          after do
            remove_request_stub(@request)
          end

          it 'returns a list of all features' do
            features = subject.list.to_a
            expect(features.count).to eq(2)
            expect(features).to include(feature1)
            expect(features).to include(feature2)
          end
        end

        context 'invalid request' do
          before do
            @request = stub_request(:get, /api\/v4\/features/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return(status: 401, body: 'unauthorized')
          end

          after do
            remove_request_stub(@request)
          end

          it 'errs with a valid error message' do
            expect do
              subject.list
            end.to raise_error(/Failed to list features: 401 -/)
          end
        end
      end

      describe '#set' do
        context 'valid request' do
          before do
            update = lambda do |request|
              feature2[:state] = 'conditional'
              feature2[:gates] = [
                { 'key' => 'boolean', 'value' => false },
                { 'key' => 'percentage_of_time', 'value' => 30 }
              ]

              { status: 201, body: feature2.to_json }
            end

            @request = stub_request(:post, /api\/v4\/features\/.*/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).to_return update
          end

          after do
            remove_request_stub(@request)
          end

          it "updates the feature's gate values" do
            subject.set(feature2[:name], 30)
            expect(feature2[:state]).to eq('conditional')
            expect(feature2[:gates]).to include({ 'key' => 'percentage_of_time', 'value' => 30 })
          end
        end

        context 'invalid request' do
          before do
            @request = stub_request(:post, /api\/v4\/features\/.*/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return(status: 401, body: 'unauthorized')
          end

          after do
            remove_request_stub(@request)
          end

          it 'errs with a valid error message' do
            expect do
              subject.set(feature2[:name], 30)
            end.to raise_error(/Failed to set feature #{feature2[:name]}: 401 -/)
          end
        end
      end
    end
  end
end
