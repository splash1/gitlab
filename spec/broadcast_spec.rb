require "spec_helper"
require "api/broadcast"


module GitLab
  module API
    describe BroadcastClient do
      context "using a stubbed server" do
        it "posts the broadcast message" do
          req = stub_request(:post, /api\/v4\/broadcast_messages/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            with(body: "message=my message&starts_at=now&ends_at=1h").
            to_return(status: 200, body: { message: "my message",
                                           starts_at: "now",
                                           ends_at: "1h",
                                           id: 1,
                                           active: true
          }.to_json)

          expect(subject.add("my message",
                                     "now",
                                     "1h")).
                  to eq("Broadcast message set to 'my message' " +
                        "from 'now' to '1h' with id 1 currently active")

          remove_request_stub(req)
        end

        it "fails to posts the broadcast message sometimes" do
          req = stub_request(:post, /api\/v4\/broadcast_messages/).
            with(body: "message=my message").
            to_return(status: 500, body: "It just failed")

          expect{ subject.add("my message", nil, nil) }
            .to raise_error /Failed to set broadcast message: 500/

          remove_request_stub(req)
        end
        it "fails to get broadcasted messages sometimes" do
          req = stub_request(:get, /api\/v4\/broadcast_messages/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return(status: 500, body: "just because")
          expect{ subject.get }.to raise_error /Failed to get broadcast messages: 500 -/
          remove_request_stub(req)
        end

        it "gets the list of broadcasted messages" do
          message_1 = { "id" => 1, "message" => "Old message", "active" => false}
          message_2 = { "id" => 2, "message" => "New message", "active" => true}

          req = stub_request(:get, /api\/v4\/broadcast_messages/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return(status: 200, body: [message_1, message_2].to_json)

          expect(subject.get).to eq([BroadcastMessage.from_hash(message_1),
                                   BroadcastMessage.from_hash(message_2)])

          remove_request_stub(req)
        end
      end
    end
  end
end
