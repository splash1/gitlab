require 'spec_helper'
require 'cog_cmd/gitlab/project/find'

describe "find project command" do
  # This is so because gitlab-ci is not a tty and cog tries to read from it failing
  before { allow(STDIN).to receive(:tty?) { true } }

  it "Works to not find a project" do
    req = stub_request(:get, /api\/v4\/projects\/.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return( status: 404 )

    with_environment("group/project") {
      finder = CogCmd::Gitlab::Project::Find.new
      finder.run_command
      expect(finder.response.content).to be_empty
    }

    remove_request_stub(req)
  end
end

